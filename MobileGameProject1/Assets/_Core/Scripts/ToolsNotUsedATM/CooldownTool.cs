﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownTool : MonoBehaviour {

    public Image cooldown;
	public bool coolingDown;
	public float cooldownTime = 5f;

	// Update is called once per frame
	void Update() {
		if (coolingDown == true) {
			//Reduce fill amount over CooldownTime seconds
			cooldown.fillAmount -= 1.0f / cooldownTime * Time.deltaTime;
		}
	}
}

