﻿using UnityEngine;
using UnityEngine.UI;

public class AimedThrowing : MonoBehaviour
{
    public int playerNumber = 1;       
    public Rigidbody shell;            
    public Transform fireTransform;    
    public Slider aimSlider;           
    public AudioSource shootingAudio;  
    public AudioClip chargingClip;     
    public AudioClip fireClip;         
    public float minLaunchForce = 3f; 
    public float maxLaunchForce = 7f; 
    public float maxChargeTime = 0.75f;
	//public PlasmaGrenadeButton plasmaGrenadeButton;
	public Image plasmaGrenadeButtonImage;
	public SlowMoManager slowMoManager;
    
    private string fireButton;         
    private float currentLaunchForce;  
    private float chargeSpeed;         
    private bool fired;


    private void Awake()
    {
        
    }

    private void OnEnable()
    {
       // m_CurrentLaunchForce = m_MinLaunchForce;
       // m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
		fireButton = "Fire"+playerNumber;
		//fireButton = plasmaGrenadeButton;
		//plasmaGrenadeButton.
        chargeSpeed = (maxLaunchForce - minLaunchForce) / maxChargeTime;
    }
    

    private void Update()
    {
		// Track the current state of the fire button and make decisions based on the current launch force.
		//aimSlider.value = minLaunchForce;


		/*
		if(currentLaunchForce >= maxLaunchForce && !fired) { // Feuern, wenn maximale Aufladunf für LaunchForce erreicht
			currentLaunchForce = maxLaunchForce;
			Invoke("Fire", 1.5f);
		}
		*/



		/*
		if (plasmaGrenadeButton.plasmaFire) { 
		//else if (Input.GetButtonDown(fireButton)) {  
		//else if (Input.GetKeyDown("space")) {         // have we pressed fire for the first time?
			fired = false;
			currentLaunchForce = minLaunchForce;

			shootingAudio.clip = chargingClip;
			shootingAudio.Play();
		}
		*/



		/*
		if (plasmaGrenadeButton.plasmaFire) { 
		//else if (Input.GetButton(fireButton) && !fired) {
		//else if (Input.GetKey("space") && !fired) {    // holding the fire button, not yet fired
			currentLaunchForce += chargeSpeed * Time.deltaTime;

			aimSlider.value = currentLaunchForce;
		}



		

		if (plasmaGrenadeButton.plasmaFire && !fired) {
			//else if (Input.GetButtonUp(fireButton) && !fired) {
			//else if (Input.GetKeyUp("space") && !fired) { // we released the button, having not fired yet
			fired = true;
			plasmaGrenadeButtonImage.fillAmount = 0;
			currentLaunchForce = maxLaunchForce;
			Invoke("Fire", .5f);
		}
		*/
    }


    private void Fire()
    {
		
		// Instantiate and launch the shell.
		Invoke("SetFiredFalse",5f);
		Rigidbody shellInstance = Instantiate(shell, fireTransform.position, fireTransform.rotation) as Rigidbody;
		shellInstance.velocity = currentLaunchForce * fireTransform.forward;
		slowMoManager.DoSlowMotion();
		shootingAudio.clip = fireClip;
		shootingAudio.Play();

		currentLaunchForce = minLaunchForce;
    }

	private void SetFiredFalse() {
		fired = false;
		plasmaGrenadeButtonImage.fillAmount = 1;
	}
}