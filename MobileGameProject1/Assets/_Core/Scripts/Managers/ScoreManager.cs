﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
	public static int score;
	public static int highscore;

	public static ScoreManager instance;			// Singelton instance 

    void Awake ()
    {
		if (instance == null) instance = this;	
    }

	private void Start() {
		score = 0;
		PlayerPrefs.SetInt("score", score);
	}


	void Update ()
    {
        
    }


	public static void AddScore( int scoreToAdd) {
		score += scoreToAdd;
	}

	public static void SaveScoreLocal() {
		PlayerPrefs.SetInt("score", score);

		if (PlayerPrefs.HasKey("highScore")) {
			if (PlayerPrefs.GetInt("highscore") < score) PlayerPrefs.SetInt("highScore", score);
		}
		else PlayerPrefs.SetInt("highScore", score);
	}

	public static void SaveScoreGooglePlay() {

	}

	public static void SaveScoreOnline() {

	}

}
