﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
	public GameObject player;
    PlayerHealth playerHealth;


	//GPlayManager gPlayManager;
    Animator anim;


    void Awake()
    {
        anim = GetComponent<Animator>();
		player = GameObject.FindWithTag("Player");
		playerHealth = player.GetComponent<PlayerHealth>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            //anim.SetTrigger("GameOver");
        }
    }

	public void NeuLaden() {
		SceneManager.LoadScene(0);
	}
}
