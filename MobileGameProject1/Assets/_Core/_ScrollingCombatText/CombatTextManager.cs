﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatTextManager : MonoBehaviour {

	public  GameObject textPrefab;
	public RectTransform combatTextCanvas;
	public float offset_y;
	public float speed;
	public float fadeTime;
	public Vector3 direction;


	private  static CombatTextManager instance;

	public static CombatTextManager Instance {
		get {
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<CombatTextManager>();
			}
			return instance;
		}
	}



// Besser vielleicht, die instanz zurückgeben und die abfragen
	private void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
	
	public  void CreateText(Canvas canv, string text, Color color, bool crit)
	{
		Vector3 offset = new Vector3(0, offset_y);
		GameObject sct = (GameObject) Instantiate(textPrefab, canv.transform.position + offset, Quaternion.identity);
		sct.transform.SetParent(canv.transform);
		sct.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		sct.GetComponent<RectTransform>().rotation = new Quaternion(0, 0, 0,0);
		sct.GetComponent<CombatText>().Initialize(speed, direction, fadeTime, crit);
		sct.GetComponent<Text>().text = text;
		sct.GetComponent<Text>().color = color;
		//sct.GetComponent
	}

}
