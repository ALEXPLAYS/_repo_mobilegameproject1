﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine;
using System;

public class VirtualShooter : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Vector3 InputDirection;
	private MoJoInputManager mojoIM;
	private Image bgImage;
	private Image shooterButton;
	Vector2 shooterButtonPosition;
	private RectTransform rectVirtualShooter;
    private float angle;
    public SteeringWheel steeringWheel;
	public bool steeringWheelActive = true;
	[SerializeField] float maxFirePerSecond=30;
	[SerializeField] float whenToSnapToWheel = 35;
	private float nextFire;

	private void Start() {
		mojoIM = MoJoInputManager.Instance;
		if (steeringWheelActive)steeringWheel = GetComponentInChildren<SteeringWheel>();
		else if(!steeringWheelActive)GetComponentInChildren<SteeringWheel>().gameObject.SetActive(false);
		mojoIM.vsFire = false;
		nextFire = Time.time;
		bgImage = GetComponent<Image>();
		shooterButton = transform.GetChild(0).GetComponent<Image>();
		shooterButtonPosition = shooterButton.rectTransform.localPosition;
		mojoIM.shooterInputDirection = InputDirection = Vector3.zero;
	}

	public virtual void OnDrag(PointerEventData eventData) {
		Vector2 pos = Vector2.zero;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform, eventData.position,eventData.pressEventCamera,out pos))
		{
			pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);
			float x = (bgImage.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
			float y = (bgImage.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;
            InputDirection = new Vector3(x, 0, y);
			InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;
			shooterButton.rectTransform.anchoredPosition = new Vector3(InputDirection.x * (bgImage.rectTransform.sizeDelta.x / 3),
				 InputDirection.z * (bgImage.rectTransform.sizeDelta.y / 3));

			mojoIM.shooterInputDirection = InputDirection;
			if (steeringWheelActive)
			{
				if(Vector2.Distance(shooterButtonPosition, shooterButton.rectTransform.localPosition) > whenToSnapToWheel)
				{
					angle = (Mathf.Atan2(InputDirection.z, InputDirection.x) * Mathf.Rad2Deg) + 180;        // Winkelberechnung für SteeringWheel
					steeringWheel.SetAngle(angle);
				}
			}
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData) {
		if(Time.time >= nextFire){	}
		mojoIM.vsFire = true;
		nextFire = Time.time + (1 / maxFirePerSecond);
	
		OnDrag(eventData);
	}

	public virtual void OnPointerUp(PointerEventData eventData) {
		mojoIM.vsFire = false;
		mojoIM.shooterInputDirection = InputDirection = Vector3.zero;
		shooterButton.rectTransform.anchoredPosition = InputDirection;
	}

}
