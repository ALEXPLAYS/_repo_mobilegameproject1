﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SteeringWheel : MonoBehaviour , IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private MoJoInputManager mojoIM;
	RectTransform rectSteeringWheelContainer;
    RectTransform rectSteeringWheelImage;
    Image steeringWheelImage;
    Camera cam;
    Vector2 pos;
	private float angle;

	public float GetAngle()
	{
		return angle;
	}

	public void SetAngle(float value)
	{
		angle = value;
	}

	void Start () {
		mojoIM = MoJoInputManager.Instance;
		rectSteeringWheelContainer = GetComponent<RectTransform>();
        rectSteeringWheelImage = transform.GetChild(0).GetComponent<RectTransform>();
		SetAngle(-90);
		mojoIM.wheelAngle = angle;
    }

    private void FixedUpdate()
    {
        rectSteeringWheelImage.rotation = Quaternion.Euler(0, 0, GetAngle() + 90);       // Rotation setzen
		mojoIM.wheelAngle = angle;
    }

    public void OnDrag(PointerEventData ped)
    { 
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rectSteeringWheelContainer, ped.position, ped.pressEventCamera, out pos))
            return;
		SetAngle((Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg)+180);
    }

    public void OnPointerDown(PointerEventData ped)
    {
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(rectSteeringWheelContainer, ped.position, ped.pressEventCamera, out pos))
            return;
		SetAngle((Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg) + 180);
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }
}
