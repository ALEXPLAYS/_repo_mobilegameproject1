﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine;
using System;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Vector3 InputDirection;
	private Image bgImage;
	private Image joystickImage;
	private MoJoInputManager mojoIM;

	private void Start() {
		bgImage = GetComponent<Image>();
		joystickImage = transform.GetChild(0).GetComponent<Image>();
		InputDirection = Vector3.zero;
		mojoIM = MoJoInputManager.Instance;
        mojoIM.joystickInputDirection = Vector3.zero;
	}

	public virtual void OnDrag(PointerEventData eventData) {
		Vector2 pos = Vector2.zero;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform, eventData.position,eventData.pressEventCamera,out pos)) {
			pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
			pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);

			float x = (bgImage.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
			float y = (bgImage.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

			InputDirection = new Vector3(x, 0, y);
			InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;
			mojoIM.joystickInputDirection = InputDirection;

			joystickImage.rectTransform.anchoredPosition = new Vector3(InputDirection.x * (bgImage.rectTransform.sizeDelta.x / 3),
				 InputDirection.z * (bgImage.rectTransform.sizeDelta.y / 3));
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData) {
		OnDrag(eventData);
	}

	public virtual void OnPointerUp(PointerEventData eventData) {
		mojoIM.joystickInputDirection = InputDirection = Vector3.zero;
		joystickImage.rectTransform.anchoredPosition = InputDirection;
	}
}
