﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoJoInputManager : MonoBehaviour {

	public  Vector3 joystickInputDirection;
    public  bool vsFire;
    public  Vector3 shooterInputDirection;
    public  float wheelAngle;


    private static MoJoInputManager instance;

	public static MoJoInputManager Instance {
		get {
			if (instance == null)
			{
				instance = FindObjectOfType<MoJoInputManager>();
			}
			return instance;
		}
	}
}
