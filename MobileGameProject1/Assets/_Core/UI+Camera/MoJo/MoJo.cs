﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoJo : MonoBehaviour {

	private MoJoInputManager mojoIM;

	void Start () {
		mojoIM = MoJoInputManager.Instance;
	}
	
	public float Horizontal()
	{
		return mojoIM.joystickInputDirection.x;
	}

	public float Vertical()
	{
		return mojoIM.joystickInputDirection.z;
	}

	public bool Fire()
	{
		return mojoIM.vsFire;
	}
}
