﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSlotSystem : MonoBehaviour {


	private static ItemSlotSystem instance;
	public static ItemSlotSystem Instance {
		get {
			if (instance == null)
			{
				instance = FindObjectOfType <ItemSlotSystem> ();
			}
			return instance;
		}
	}


	public List<ItemSlot> itemSlots;
	public int slotCount = 0;
	public Transform onDragParentTransform;


	void Start () {
		onDragParentTransform = FindObjectOfType<SlotPanel>().transform;
	}

	void Update () {
		
	}

	public int AddItemSlot(ItemSlot slot)
	{
		itemSlots.Add(slot);
		slotCount = itemSlots.Count;
		return slotCount-1;
	}

}
