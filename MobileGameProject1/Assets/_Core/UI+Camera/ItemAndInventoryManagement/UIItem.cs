﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;
using UnityEngine.EventSystems;


public class UIItem : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

	public Transform onDragParent;
	public Transform onDragEndParent;
	public Transform slotPanel;
	public int offset;

	void Start () {
		slotPanel=FindObjectOfType<SlotPanel>().transform;
		onDragParent = slotPanel;
		onDragEndParent = transform.parent;
	}


	public void OnBeginDrag(PointerEventData eventData)
	{
		transform.SetParent(onDragParent) ;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		transform.position = transform.position + new Vector3 (-offset, 0, offset);
	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = (eventData.position) + new Vector2 (-offset,offset);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		
		transform.SetParent(onDragEndParent);
		transform.position = onDragEndParent.position;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}


}
