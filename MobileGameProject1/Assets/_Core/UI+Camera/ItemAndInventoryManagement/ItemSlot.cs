﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IDropHandler {

	public int id;
	private ItemSlotSystem iss;



	void Start () {

		id = ItemSlotSystem.Instance.AddItemSlot(this);
	}
	
	void Update () {
		
	}

	public void OnDrop(PointerEventData eventData)
	{
		
		UIItem uiItem = eventData.pointerDrag.GetComponent<UIItem>();
		Debug.Log("Item Dropped - Slot: " + gameObject.name);
		uiItem.onDragEndParent = transform;
		uiItem.transform.position = transform.position;
	}
}
