﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public LayerMask shootableMask;
    public ParticleSystem explosionParticles;       
    public AudioSource explosionAudio;              
    public float maxDamage = 100f;                  
    public float explosionForce = 1000f;            
    public float maxLifeTime = 2f;                  
    public float explosionRadius = 5f;              


    private void Start()
    {
        Destroy(gameObject, maxLifeTime);
    }


    private void OnTriggerEnter(Collider other)
    {
		// Find all the enemys in an area around the grenade and damage them.
		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, shootableMask);
		for(int i = 0; i < colliders.Length; i++) {
			Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

			if (!targetRigidbody) continue;

			targetRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);

			EnemyHealth targetHealth = targetRigidbody.GetComponent<EnemyHealth>();

			if (!targetHealth) continue;

			float damage = CalculateDamage(targetRigidbody.position);

			targetHealth.TakeDamage(damage,targetRigidbody.position);

			//Debug.Log("Habe gegner nummer: " + i + " bearbeitet.");
		}

		explosionParticles.transform.parent = null; // Partiket von der Granate lösen, damir die weiterlaufen
		explosionParticles.Play();                  // Partikel abspielen
		explosionAudio.Play();                      // explosionAudio abspielen

		//Destroy(explosionParticles.gameObject, explosionParticles.duration);
		//Destroy(gameObject);
    }


    private float CalculateDamage(Vector3 targetPosition)
    {
		// Calculate the amount of damage a target should take based on it's position.
		Vector3 explosionToTarget = targetPosition - transform.position;
		float explosionDiustance = explosionToTarget.magnitude;
		float relativeDistance = (explosionRadius - explosionDiustance) / explosionRadius;
		float damage = relativeDistance * maxDamage;
		damage = Mathf.Max(0f, damage);
        return damage;
    }
}