﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastAbilityAction : MonoBehaviour {

    int damagePerShot;
    float timeBetweenShots;
    float weaponRange;
    float hitForce;
    LayerMask shootableMask;
    Transform gunEnd;
    private LineRenderer laserLine;  
    private WaitForSeconds shotDuration = new WaitForSeconds(.05f);


    public AudioClip fireSound;
    private AudioSource gunAudio;
    
    private float nextFire;

    public void Initialize(RaycastAbility raycastAbility)
    {
        gunEnd = transform;
        laserLine = GetComponent<LineRenderer>();
        laserLine.material = new Material(Shader.Find("Unlit/Color"));
        laserLine.material.color = raycastAbility.laserColor;
        timeBetweenShots = raycastAbility.abilityBaseCD;
        damagePerShot = raycastAbility.damagePerShot;
        weaponRange = raycastAbility.weaponRange;
        hitForce = raycastAbility.hitForce;
        shootableMask = raycastAbility.shootableMask;
        gunAudio = GetComponent<AudioSource>();
        fireSound = raycastAbility.abilitySound;
        gunAudio.clip = fireSound;
    }

	
	public void Fire () {
        //if (MyInputManager.vsFire && Time.time > nextFire)
        if(Time.time > nextFire)
        {
            //Debug.Log("Schiesse Ability triggered");
            nextFire = Time.time + timeBetweenShots;
            StartCoroutine(ShotEffect());
            Vector3 rayOrigin = gunEnd.position;
            // DEBUG schusslinie zeichnen
            //Debug.DrawRay(rayOrigin, gunEnd.forward * weaponRange, Color.yellow);
            // Declare a Raycast hit to store Information about what the raycast hits
            RaycastHit hit;
            // set the Start Position for visual Laser Effect (at the gunEnd)
            laserLine.SetPosition(0, gunEnd.position);

            if(Physics.Raycast(rayOrigin, gunEnd.forward, out hit, weaponRange, shootableMask))     // Wenn der Raycast etwas trifft
            {
                laserLine.SetPosition(1, hit.point);                                    // Endpunkt vom Laser setzen
                Component damagableComponent  = hit.collider.GetComponent(typeof(IDamagable));
                if (damagableComponent)                                                // Wenn RaycastZiel ein enemyHealth Script hat
                {
                    (damagableComponent as IDamagable).TakeDamage(damagePerShot, hit.point);                   // Füge Schaden zu
                }
                if (hit.rigidbody != null)                                              // Wenn RaycastZiel ein Rigidbody hat
                {
                    hit.rigidbody.AddForce(-hit.normal * hitForce);                     // Dann Füge Rückstosseffekt (Force) hinzu
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + (transform.forward * weaponRange));
            }
        }
	}

	public void NoMoreFire()
	{
		return;
	}


    private IEnumerator ShotEffect()
    {
        gunAudio.Play();
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }
}
