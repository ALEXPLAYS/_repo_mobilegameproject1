﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityDirectTrigger : MonoBehaviour {

	private MoJoInputManager mojoIM;

	public Ability ability;
	private float cooldownTime;
	private float nextTrigger;
	private bool firing;


	void Start () {
		mojoIM = MoJoInputManager.Instance;
		ability.Initialize(gameObject);
		cooldownTime = ability.abilityBaseCD;
    }
	
	void Update () {

        if (Time.time >= nextTrigger && mojoIM.vsFire)
        {
			nextTrigger = Time.time + cooldownTime;
            ability.TriggerAbility();
			firing = true;
        }

		if (!mojoIM.vsFire && firing==true)
		{
			firing = false;
			ability.NoMoreFire();
		}
	}
}
