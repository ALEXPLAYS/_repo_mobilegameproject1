﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject {

    public string abilityName = "New Ability";
    public Sprite abilityIcon;
    public int energyCost = 10;
    public AudioClip abilitySound;
    public float abilityBaseCD = 1f;
    public LayerMask shootableMask;

    public abstract void Initialize (GameObject obj);
    public abstract void TriggerAbility();
	public abstract void NoMoreFire();
}
