﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrahlColliderAbilityAction : MonoBehaviour {

	StrahlColliderAbility scAbility;

    float hitForce;
	private ParticleSystem strahl;
	private Collider dmgCollider;
    private LayerMask shootableMask;
    private Transform gunEnd;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.5f);
    public AudioClip fireSound;
    private AudioSource audioSource;
    private float nextFire;
	private string abilityName;
	private int dotFxTickDmg;
	private float dotFxInterval;
	private float weaponRange = 50f;
	private float dotFxDuration;
	private Transform vfxDotPrefab;

	public void Initialize(StrahlColliderAbility scAbility)
    {
		abilityName = scAbility.abilityName;
		dotFxTickDmg = scAbility.dotFxTickDmg;
		dotFxInterval = scAbility.dotFxInterval;
		weaponRange = scAbility.weaponRange;
		dotFxDuration = scAbility.dotFxDuration;
		vfxDotPrefab = scAbility.vfxDotPrefab;
		dmgCollider = GetComponent<BoxCollider>();
		dmgCollider.enabled = false;
		gunEnd = GetComponentInChildren<GunEnd>().transform;
		strahl = GetComponentInChildren<ParticleSystem>();
        shootableMask = scAbility.shootableMask;
        audioSource = GetComponent<AudioSource>();
        fireSound = scAbility.abilitySound;
        audioSource.clip = fireSound;
		
		GenerateDDLOL();
	}

	public void GenerateDDLOL ()
	{
		DamageDataLOL ddLOL=gameObject.AddComponent<DamageDataLOL>();
		ddLOL.abilityName = abilityName;
		ddLOL.singleDamage = 0;
		ddLOL.dotFxTickDmg = dotFxTickDmg;
		ddLOL.dotUptime = 0;
		ddLOL.dotFxDuration = dotFxDuration;
		ddLOL.dotFxTicks = 0;
		ddLOL.dotFxInterval = dotFxInterval;
		ddLOL.hitForce = hitForce;
		ddLOL.aoeDamageRange = 0;
		ddLOL.vfxDotPrefab = vfxDotPrefab;
	}

	
	public void Fire () {
        if(Time.time >= nextFire)
        {
			nextFire = Time.time + 1.5f;
            StartCoroutine(ShotEffect());
			strahl.Play();
		}
	}

	public void NoMoreFire()
	{
		Invoke("DisableDmgCollider", 3f);
	}

	private void DisableDmgCollider()
	{
		dmgCollider.enabled = false;
		Debug.Log("DMG-COLLIDER DISABLED");
	}

	private IEnumerator ShotEffect()
    {
        audioSource.Play();
        yield return shotDuration;
		dmgCollider.enabled = true;
    }


}
