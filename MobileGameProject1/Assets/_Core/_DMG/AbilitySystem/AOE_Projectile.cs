﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOE_Projectile : MonoBehaviour {

    public float explosionForce;
    public float explosionRadius;
    public float maxDamage;
    public ParticleSystem impactExplosionParticles;
    public ParticleSystem launchParticles;
    public ParticleSystem flightParticles;
    public AudioClip impactExplosionAudio;
    public AudioClip launchAudio;
    public AudioClip flightAudio;
    public LayerMask shootableMask;
    public AudioSource audioSource;
    public GameObject player;
    


    void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = launchAudio;
        audioSource.Play();
        //impactExplosionParticles=GetComponent<imp>
        //launchParticles.Play();
    }
	
	void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTriggerEnter");
        // Find all the enemys in an area around the grenade and damage them.
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, shootableMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();   // Hier noch eventuell Interface reinbringen

            if (!targetRigidbody) continue;

            targetRigidbody.AddExplosionForce(explosionForce, transform.position, explosionRadius);

            EnemyHealth targetHealth = targetRigidbody.GetComponent<EnemyHealth>();

            if (!targetHealth) continue;

            float damage = CalculateDamage(targetRigidbody.position);

            targetHealth.TakeDamage(damage, targetRigidbody.position);

            Debug.Log("Habe gegner nummer: " + i + " bearbeitet.");
        }

        impactExplosionParticles.transform.parent = null; // Partiket von der Granate lösen, damir die weiterlaufen
        impactExplosionParticles.Play();                  // Partikel abspielen
        audioSource.clip = impactExplosionAudio;          // Audioclip für Explosion
        audioSource.Play();                                 // explosionAudio abspielen
        Destroy(impactExplosionParticles.gameObject, impactExplosionParticles.main.duration);
        audioSource.transform.parent = null;

        //Disable Renderer and Collider, so that Audio can play till end but projectile no longer visible
        DisableComponents();
        // Destroy Projectile when audioClip has Played
        Destroy(gameObject,audioSource.clip.length);
    }


    void DisableComponents()
    {
        transform.GetComponent<SphereCollider>().enabled = false;
        transform.GetComponent<MeshRenderer>().enabled = false;
    }

    void EnableComponents()
    {

    }



    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
        Vector3 explosionToTarget = targetPosition - transform.position;
        float explosionDiustance = explosionToTarget.magnitude;
        float relativeDistance = (explosionRadius - explosionDiustance) / explosionRadius;
        float damage = relativeDistance * maxDamage;
        damage = Mathf.Max(0f, damage);
        return damage;
    }


}
