﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAoeImpactAbilityAction : MonoBehaviour {

    [HideInInspector] public float maxAOEDamagePerTarget = 10f;
    [HideInInspector] public float explosionForce = 1000f;
    [HideInInspector] public float projectileLifetime = 2f;
    [HideInInspector] public float aoeDamageRadius = 5f;
    [HideInInspector] public float projectileLaunchForce = 500f;
    [HideInInspector] public GameObject projectileToShoot;
    string abilityName;
    public Transform gunEnd;

    public void Initialize(ProjectileAoeImpactAbility pAOEIAbility)
    {
        gunEnd = GetComponentInChildren<GunEnd>().transform;

        maxAOEDamagePerTarget = pAOEIAbility.maxAOEDamagePerTarget;
        explosionForce = pAOEIAbility.explosionForce ;
        projectileLifetime = pAOEIAbility.projectileLifetime;
        aoeDamageRadius = pAOEIAbility.aoeDamageRadius;
        projectileLaunchForce = pAOEIAbility.projectileLaunchForce;
        projectileToShoot = pAOEIAbility.projectileToShoot;
        abilityName = pAOEIAbility.abilityName;
    }

    public void fire()
    {
        Debug.Log("USED"+abilityName);
        GameObject projectileClone = (GameObject)Instantiate(projectileToShoot, gunEnd.position, gunEnd.rotation);
        projectileClone.GetComponent<Rigidbody>().AddForce(gunEnd.forward * projectileLaunchForce);
    }

	public void NoMoreFire()
	{
		return;
	}

}
