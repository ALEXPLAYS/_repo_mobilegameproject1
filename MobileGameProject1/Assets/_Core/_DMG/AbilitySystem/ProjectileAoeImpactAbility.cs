﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = ("Milipu/Ability/ProjectileAoeImpactAbility"))]
public class ProjectileAoeImpactAbility : Ability
{
    [Header("AOE-Ability")]
    public float maxAOEDamagePerTarget = 10f;
    public float explosionForce = 1000f;
    public float projectileLifetime = 2f;
    public float aoeDamageRadius = 5f;
    public float projectileLaunchForce = 500f;
    public GameObject projectileToShoot;

    private ProjectileAoeImpactAbilityAction pAOEIAbility;

    public override void Initialize(GameObject obj)
    {
        
        pAOEIAbility = obj.AddComponent<ProjectileAoeImpactAbilityAction>();
        Debug.Log("Initialize of Ability " + pAOEIAbility.name);
        pAOEIAbility.Initialize(this);
    }

	public override void NoMoreFire()
	{
		pAOEIAbility.NoMoreFire();
	}

	public override void TriggerAbility()
    {
        pAOEIAbility.fire();
    }
}
