﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Milipu/Abilities/StrahlColliderAbility")]

public class StrahlColliderAbility : Ability {

    public int dotFxTickDmg;
	public float dotFxInterval;
    public float weaponRange = 50f;
	public float dotFxDuration;
	public ParticleSystem strahl;
	public Transform strahlCollider;
	public Transform vfxDotPrefab;
	public float hitForce = 100f;
    //public Color laserColor = Color.white;

    private StrahlColliderAbilityAction scAbilityAction;        // Monobehaviour to shoot


    public override void Initialize(GameObject obj)
    {
		scAbilityAction = obj.AddComponent<StrahlColliderAbilityAction>();
		scAbilityAction.Initialize(this);
    }

	public override void NoMoreFire()
	{
		scAbilityAction.NoMoreFire();
	}

	public override void TriggerAbility()
    {
        scAbilityAction.Fire();
    }

}
