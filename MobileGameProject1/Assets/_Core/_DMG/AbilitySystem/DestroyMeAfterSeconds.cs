﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMeAfterSeconds: MonoBehaviour {

    [SerializeField] float destroyMeAfterSeconds=3f;

	// Use this for initialization
	void Start () {
        Invoke("Die", destroyMeAfterSeconds);
	}


    void Die()
    {
        Destroy(gameObject);
    }
}
