﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Milipu/Abilities/RaycastAbility")]

public class RaycastAbility : Ability {

    public int damagePerShot = 1;
    public float weaponRange = 50f;
    public float hitForce = 100f;
    public Color laserColor = Color.white;

    private RaycastAbilityAction rcAbilityAction;        // Monobehaviour to shoot



    public override void Initialize(GameObject obj)
    {
        obj.AddComponent<RaycastAbilityAction>();
        rcAbilityAction = obj.GetComponent<RaycastAbilityAction>();
        rcAbilityAction.Initialize(this);
    }

	public override void NoMoreFire()
	{
		rcAbilityAction.NoMoreFire();
	}

	public override void TriggerAbility()
    {
        rcAbilityAction.Fire();
    }

}
