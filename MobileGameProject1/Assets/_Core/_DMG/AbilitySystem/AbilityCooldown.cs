﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AbilityCooldown : MonoBehaviour ,IPointerUpHandler, IPointerDownHandler
{
    public Ability ability;

    private bool buttonPressed;
    public Image darkMask;
    public Text coolDownText;
    public Image buttonSpriteAbilityIcon;

    private AudioSource abilitySoundSource;
    private float coolDownDuration;
    private float nextReadyTime;
    private float coolDownTimeLeft;
    GameObject player;

    private void Awake()
    {

    }


    void Start () {

        //buttonSpriteAbilityIcon = GetComponent<Image>();
        //darkMask = GetComponentInChildren<Image>();
        //coolDownText = GetComponentInChildren<Text>();
        buttonPressed = false;
        player = GameObject.FindWithTag("Player");
        //Initialize(ability);
	}


    public void Initialize(Ability iniAbility)
    {
        ability = iniAbility;
        buttonSpriteAbilityIcon.sprite = ability.abilityIcon;
        abilitySoundSource = GetComponent<AudioSource>();
        darkMask.sprite = ability.abilityIcon;
        coolDownDuration = ability.abilityBaseCD;

        AbilityReady();
    }


    void Update () {

        bool coolDownComplete = (Time.time > nextReadyTime);
        if (coolDownComplete)
        {
            AbilityReady();
            if (buttonPressed)
            {
                ButtonTriggered();
            }
        }
        else
        {
            Cooldown();
        }
	}


    private void AbilityReady()
    {
        coolDownText.enabled = false;
        darkMask.enabled = false;
    }



    private void Cooldown()
    {
        coolDownTimeLeft -= Time.deltaTime;
        float roundedCD = Mathf.Round(coolDownTimeLeft);
        coolDownText.text = roundedCD.ToString();
        darkMask.fillAmount = (coolDownTimeLeft / coolDownDuration);
    }

    private void ButtonTriggered()                              // Special Ability Ausführen
    {
        var energyComponent = player.GetComponent<Energy>();
        //var playerHealth = player.GetComponent<PlayerHealth>();
        var energyCost = ability.energyCost;
        if (energyComponent.IsEnergyAvailable(energyCost))
        {
            energyComponent.consumeEnergy(energyCost);
            nextReadyTime = coolDownDuration + Time.time;
            coolDownTimeLeft = coolDownDuration;
            darkMask.enabled = true;
            coolDownText.enabled = true;

            abilitySoundSource.clip = ability.abilitySound;
            abilitySoundSource.Play();
            Invoke("UseAbility", .8f);
        }
    }


    private void UseAbility()
    {
        ability.TriggerAbility();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonPressed = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonPressed = true;
    }


}
