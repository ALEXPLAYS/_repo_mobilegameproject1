﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDataLOL : MonoBehaviour {

	public string abilityName;
	public float singleDamage;
	public float dotFxTickDmg;
	public float dotUptime;
	public float dotFxDuration;
	public float dotFxTicks;
	public float dotFxInterval;
	public float hitForce;
	public float aoeDamageRange;
	public bool isAvoidable;
	public float hasModifier;

	public Transform vfxDotPrefab;

}
