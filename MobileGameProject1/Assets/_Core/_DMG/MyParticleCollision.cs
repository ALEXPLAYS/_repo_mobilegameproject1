﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyParticleCollision : MonoBehaviour {

    [SerializeField] float damageToPlayerPerTick = 10;
    [SerializeField] float timeBetweenTicks = .5f;
    float nextTick;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnParticleCollision(GameObject other)
    {
        if (Time.time > nextTick)
        {
            nextTick = Time.time + timeBetweenTicks;
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            Vector3 hitPoint = other.transform.position;
            if (playerHealth != null)
            {
                if (playerHealth.currentHealth > 0)
                {
                    playerHealth.TakeDamage(damageToPlayerPerTick, hitPoint);
                } 
            }
        }
    }
}
