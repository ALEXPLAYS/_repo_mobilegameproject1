﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShooting : MonoBehaviour {

	private MoJoInputManager mojoIM;
	
	public int damagePerShot = 10;
    public float timeBetweenShots = .25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;
    public AudioClip fireSound;
    public LayerMask shootableMask;

    private WaitForSeconds shotDuration = new WaitForSeconds(.05f);
    private AudioSource gunAudio;
    private LineRenderer laserLine;
    private float nextFire;

    public void Initialize()
    {
        
    }


    void Start () {
		mojoIM = MoJoInputManager.Instance;

		laserLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        gunAudio.clip = fireSound;
	}
	
	void Update () {
		if (mojoIM.vsFire && Time.time > nextFire)
        {
            Debug.Log("Schiesse");
            nextFire = Time.time + timeBetweenShots;
            StartCoroutine(ShotEffect());
            Vector3 rayOrigin = transform.position;
            RaycastHit hit;
            laserLine.SetPosition(0, transform.position);
            if(Physics.Raycast(rayOrigin, transform.forward, out hit, weaponRange, shootableMask))     // Wenn der Raycast etwas trifft
            {
                laserLine.SetPosition(1, hit.point);                                    // Endpunkt vom Laser setzen
                Component damagableComponent  = hit.collider.GetComponent(typeof(IDamagable));
                if (damagableComponent)                                                // Wenn RaycastZiel ein enemyHealth Script hat
                {
                    (damagableComponent as IDamagable).TakeDamage(damagePerShot, hit.point);                   // Füge Schaden zu
                }
                if (hit.rigidbody != null)                                              // Wenn RaycastZiel ein Rigidbody hat
                {
                    hit.rigidbody.AddForce(-hit.normal * hitForce);                     // Dann Füge Rückstosseffekt (Force) hinzu
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + (transform.forward * weaponRange));
            }
        }
	}

    private IEnumerator ShotEffect()
    {
        gunAudio.Play();
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }
}
