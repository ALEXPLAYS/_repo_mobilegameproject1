﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Milipu/Weapon"))]
public class Weapon : ScriptableObject {

    public Transform gripTransform;
    [SerializeField] Transform gunEnd;
    [SerializeField] GameObject weaponPrefab;
    [SerializeField] AnimationClip attackAnimation;

    public GameObject GetWeaponPrefab()
    {
        return weaponPrefab;
    }

    public Transform GetGunEnd()
    {
        return gunEnd;
    }
}
