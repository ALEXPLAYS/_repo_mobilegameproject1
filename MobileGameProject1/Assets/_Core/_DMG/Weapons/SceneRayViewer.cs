﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRayViewer : MonoBehaviour {

    public float weaponRange = 50f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 lineOrigin = transform.position;
        Debug.DrawRay(lineOrigin, transform.forward * weaponRange, Color.green);
	}
}
