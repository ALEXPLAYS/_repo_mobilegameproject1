﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleHPBar : MonoBehaviour {

	// Das Script ist nur zum anpassen des HP-BAR Durchmessers



	public float barRadius;
	private RectTransform healthBar;
	// Use this for initialization
	

	private void Awake() {
		healthBar = GetComponent<RectTransform>();
	}

	void Start () {
		healthBar.localScale = new Vector3(barRadius, barRadius, 1f);//This works
	}
	// Update is called once per frame
	void Update () {
		
	}
}
