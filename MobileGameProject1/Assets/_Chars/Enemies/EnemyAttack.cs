﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;
	[SerializeField] float attackRadius = 2f;
 
    Animator anim;
    GameObject player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    [SerializeField] bool playerInRange;
    float timer;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
    }

    /// <summary>
    /// Dieses Ganze AGGRO System kann man sooo toll anpassen, auch für stealth Games...
    /// Da kann man einfach so verdammt viel mit machen, auch mal zusammen belabern was
    /// man da so machen kann
    /// 
    /// 
    /// </summary>
    /// 


    void OnTriggerEnter (Collider other)
    {
		/*
        if(other.gameObject == player)                  // Das hier war die veraltete Version mit Collidern
        {                                               // Mit Raycasts ist das besser gewesen, obwohl es auch für 
            playerInRange = true;                       // Die ColliderMethode einsatzgebiete gibt (stealth-game)
        }                                               // Beides fertigprogrammieren und einfach die möglichkeit 
		*/                                              // zur Auswahl geben
    }


    void OnTriggerExit (Collider other)
    {
		/*
        if(other.gameObject == player)
        {
            playerInRange = false;
        }
		*/
    }


    void Update ()
    {
        timer += Time.deltaTime;
		float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);   // Spieler Entfernung ermitteln

		if (distanceToPlayer <= attackRadius) playerInRange = true;									// Player in attackRange = true
		else playerInRange = false;																	// Player in attackRange=false;

		if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack ();
        }

        if(playerHealth.currentHealth <= 0)     // Keine Ahnung warum ich das hier reingemacht habe,,, muss ich nachschauen
        {
            anim.SetTrigger ("PlayerDead");
        }
    }


    void  Attack ()
    {
        timer = 0f;
        anim.SetTrigger("Attack");
    }

    void DamagePlayer()                             // Eigentlich noch mit Interfaces machen.... Auf allen ebenen :/
    {
        if (playerHealth.currentHealth > 0)
        {
            Component damagableComponent = playerHealth.GetComponent(typeof(IDamagable));
            if (damagableComponent)
            {
                (damagableComponent as IDamagable).TakeDamage(attackDamage, new Vector3(0, 0, 0));
            }
        }
    }


	void OnDrawGizmos() {

		// Draw attackSphere
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, attackRadius);

		//  // Draw runAway Sphere
		// Gizmos.color = Color.black;
		//  Gizmos.DrawWireSphere(transform.position, rennWeg);
	}



}
