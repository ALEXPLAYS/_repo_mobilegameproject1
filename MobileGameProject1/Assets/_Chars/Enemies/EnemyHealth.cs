﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour, IDamagable
{
    public float maxHealthPoints = 100;
    public float currentHealth;
    public float healthAsPercentage { get { return currentHealth / maxHealthPoints; } }
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip audioDeath;
    public AudioClip audioGetHit;
    public AudioClip audioGetCriticalHit;

    // für die Runde Health bar
    public Slider healthSlider;
	public Image fillImage;
	public Color fullHealthColor = Color.green;
	public Color zeroHealthColor = Color.red;
    public ParticleSystem hitParticles;
	public ParticleSystem vfxDot;
	private bool hasDotVfx;
	public Canvas sctCanvas;
	CombatTextManager sct;

	Animator anim;
    AudioSource enemyAudio;
    bool isDead;
    bool isSinking;
	private float nextDmgTime;

	public bool crDotRefresh;
	public bool hasCoroutineDot;
	private IEnumerator coroutineDOT;



	void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        currentHealth = maxHealthPoints;
		crDotRefresh = false;
		hasCoroutineDot = false;
		SetHealthUI();  // HealthBar initialisieren
		sct = CombatTextManager.Instance;
	}


    void Update ()
    {
        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }

	void IniDDLOL(DamageDataLOL ddLOL)
	{

	}


    public void TakeDamage (float amount, Vector3 hitPoint)
    {
        if(isDead)
            return;
        enemyAudio.clip = audioGetHit;
        enemyAudio.Play ();
        currentHealth = Mathf.Clamp(currentHealth - amount, 0f, maxHealthPoints);
		SetHealthUI(); // HP Balken updaten (Amount and Color)
		MakeScrollingDamageText (amount);
		//hitParticles.transform.position = transform.position;
        hitParticles.Play();
        if(currentHealth <= 0)
        {
            Death ();
        }
    }


	public void TakeDamage(DamageDataLOL ddLOL)
	{
		if (isDead)
			return;
		enemyAudio.clip = audioGetHit;
		enemyAudio.Play();
		var calculatedDamage = GetCalculatedDamage(ddLOL);
		currentHealth = Mathf.Clamp(currentHealth - calculatedDamage, 0f, maxHealthPoints);
		SetHealthUI(); 
		MakeScrollingDamageText(calculatedDamage);

		if ( ! hasDotVfx) // Wenn DOT aber noch kein VFX läuft, hier holen und Childen
		{
			MakeDotVfx(ddLOL);
		}
		
		if (currentHealth <= 0)
		{
			Death();
		}
	}

	public int GetCalculatedDamage(DamageDataLOL ddLOL)
	{
		float baseTick = ddLOL.dotFxTickDmg;
		float dmgToAdd = Random.Range((baseTick / 10 / 5), baseTick / 10);
		float dotFxTickDmg = Mathf.Round(baseTick + dmgToAdd);
		return (int) dotFxTickDmg;
	}



	public void MakeDotVfx (DamageDataLOL ddLOL)
	{
		Transform vfxPrefabDot = Instantiate(ddLOL.vfxDotPrefab, transform.position, Quaternion.identity);
		vfxPrefabDot.SetParent(gameObject.transform);
		vfxDot = vfxPrefabDot.GetComponent<ParticleSystem>();
		vfxDot.Play();
		hasDotVfx = true;
	}

	public void KillDotVfx()
	{
		if (!hasDotVfx) return;
		//if( hasDotVfx && vfxDot!=null)Destroy(vfxDot.gameObject, 2f);
	}


	/*
	public void MakeHitVfx(DamageDataLOL ddLOL)
	{		
			hitParticles.transform.position = transform.position;
			//hitParticles.Play();
			Transform vfxPrefabDot = Instantiate(ddLOL.vfxPrefabDot, transform.position, Quaternion.identity);
			vfxPrefabDot.SetParent(gameObject.transform);
			vfxDot = vfxPrefabDot.GetComponent<ParticleSystem>();
			vfxDot.Play();
			hasDotVfx = true;
	}

	public void KillVfxHit()
	{
		Destroy(vfxDot.gameObject, 2f);
	}
	*/


	public void MakeScrollingDamageText(float amount)
	{
		bool crit = true;
		sct.CreateText(sctCanvas, Mathf.Round(amount).ToString(), Color.red, crit);
	}



	void Death ()
    {
		CoroutineDotKill();
		KillDotVfx();
		hasCoroutineDot = false;
		isDead = true;
		ScoreManager.AddScore (scoreValue);
        anim.SetTrigger ("Death");
		
		enemyAudio.PlayOneShot(audioDeath, 0.7f);
        Invoke("StartSinking", 4f);
    }



	private void SetHealthUI() {            // Enemy HP Bar updaten (Farbe und Value)
		
		healthSlider.value = healthAsPercentage*100;
		fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, healthAsPercentage);
	}


    
	public void StartSinking ()
    {
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        Destroy (gameObject, 2f);
    }




	private void OnTriggerEnter(Collider other)
	{
		DamageDataLOL ddLOL = other.GetComponent<DamageDataLOL>();
		if (ddLOL != null && Time.time >= nextDmgTime)
		{
			if (hasCoroutineDot)
			{
				crDotRefresh = true;
			}
			else if ( ! hasCoroutineDot)
			{
				CoroutineDotStart(ddLOL);
			}
		}
	}


	private void OnTriggerStay(Collider other)
	{
		DamageDataLOL ddLOL = other.GetComponent<DamageDataLOL>();
		if (ddLOL != null && Time.time >= nextDmgTime)
		{
			if (hasCoroutineDot)
			{
				crDotRefresh = true;
			}
			else if ( ! hasCoroutineDot)
			{
				CoroutineDotStart(ddLOL);
			}
		}
	}



	private void OnTriggerExit(Collider other)
	{
		DamageDataLOL ddLOL = other.GetComponent<DamageDataLOL>();
		if (ddLOL != null && Time.time >= nextDmgTime)
		{
			if (hasCoroutineDot)
			{
				crDotRefresh = true;
			}
			else if (!hasCoroutineDot)
			{
				CoroutineDotStart(ddLOL);
			}
		}
	}

	void CoroutineDotStart(DamageDataLOL ddLOL)
	{
		coroutineDOT = DamageOverTime(ddLOL);
		StartCoroutine(coroutineDOT);
	}

	void CoroutineDotKill()
	{
		if (hasCoroutineDot)
		{
			StopCoroutine(coroutineDOT);
			hasCoroutineDot = false;
		}	
	}



	// Coroutine für die Berechnung von DOT's und anderen längeren Effekten
	public IEnumerator DamageOverTime (DamageDataLOL dd)
	{
		DamageDataLOL ddLOL= dd;
		float dotFxDuration = ddLOL.dotFxDuration;
		float dotFxTicks = ddLOL.dotFxTicks;
		float dotFxInterval = ddLOL.dotFxInterval;
		if (dotFxTicks < (dotFxDuration / dotFxInterval)) dotFxTicks = Mathf.Round(dotFxDuration / dotFxInterval);
		int i = 1;
		dotFxTicks = 10;
		while (i <= dotFxTicks)
		{
			hasCoroutineDot = true;
			if (crDotRefresh)
			{
				Debug.Log("Coroutine Dot Refresh.");
				i = 1;
				crDotRefresh = false;
			}
			TakeDamage(ddLOL);
			Debug.Log("Ich ticke runter.. noch " + (10 - i) + "mal - Ich bin Gegner: " + gameObject.name);
			i++;
			yield return new WaitForSeconds(dotFxInterval);
		}
		if (i > dotFxTicks)
		{
			hasCoroutineDot = false;
			KillDotVfx();
		}
	}

}
