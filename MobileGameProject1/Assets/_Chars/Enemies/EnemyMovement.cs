﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
	public bool returnToStartPositionWhenNotAggro;

	[SerializeField] float chaseRadius = 5f;

	Animator anim;
	Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;
	Vector3 startPosition;
    private bool isAtStartPosition;               // Wenn Mob zurück soll und dort wieder angekommen ist;
    private bool isAggro;                          // Wenn mob Aggro auf Player ist weil er angeschossen wurde aber ausser Reichweite wäre
    [SerializeField] float aggroTime = 5f;          // Wie lange ist mob Aggro nach detztem Hit?


    void Awake ()																	// Wenn Enemy geladen wird
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;				// Spieler GameObject finden
        playerHealth = player.GetComponent <PlayerHealth> ();						// Referenz auf PlayerHealth Script
        enemyHealth = GetComponent <EnemyHealth> ();								// Referenz auf EnemyHealth Script
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();						// NavMeshAgent Referenzieren
		startPosition = transform.position;											// Enemy Start Position merken, falls er zurückkehren soll
		anim = GetComponent<Animator>();											// Animator Referenz
    }


    void Update ()
    {
		if (enemyHealth.currentHealth >0 && playerHealth.currentHealth >0) {							// Wenn Gegner und Spieler leben

			float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);	// Entfernung zum Spieler prüfen
            if (returnToStartPositionWhenNotAggro)
            {
                float distanceToStart = Vector3.Distance(transform.position, startPosition );	        // Entfernung zur StartPosi prüfen
                if (distanceToStart < .1f && isAtStartPosition==false)
                {
                    //Debug.Log("Wieder am Start");
                    isAtStartPosition = true;                                                           // isAtStartPosition true wenn am Start ist
                    transform.LookAt(player.transform.position);                                                 // Richtung Spieler schauen wenn am Startpunkt
                }
                else if (distanceToStart > 1f) isAtStartPosition = false;
            }

            if (distanceToPlayer <= chaseRadius ) {														// Spieler innerhalb AGGRO-Range?
				nav.SetDestination(player.position);                                                    // Dann Spieler als Ziel setzen   
                anim.SetTrigger("Walk");                                                                // Animation WALK triggern
            }

			else {																						// Wenn beide leben aber Spieler nicht in AGGRO Range
				if (returnToStartPositionWhenNotAggro) {												// Soll Gegner zurück zur Startposition?
                    if (!isAtStartPosition)
                    {
                        nav.SetDestination(startPosition);                                                  // Startposition als Ziel setzen
                        anim.SetTrigger("Walk");
                    }
                    else if (isAtStartPosition) anim.SetTrigger("Idle");
                }

				else {                                                                                  // Wenn nicht zum Start

					nav.SetDestination(transform.position);												// Aktuelle Position als Ziel
                    anim.SetTrigger("Idle");                                                            // Trigger IDLE Animation
                }
			}

			
		}

		else if (enemyHealth.currentHealth <= 0 || playerHealth.currentHealth <= 0) {                   // Wenn Spieler oder Gegner tot

			nav.enabled = false;																		// dann Navigation ausschalten
		}

    }


	void OnDrawGizmos() {

		// Draw attackSphere
		//Gizmos.color = Color.red;
		//Gizmos.DrawWireSphere(transform.position, attackRadius);

		// Draw chase Sphere
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(transform.position, chaseRadius);

		//  // Draw runAway Sphere
		// Gizmos.color = Color.black;
		//  Gizmos.DrawWireSphere(transform.position, rennWeg);
	}


}
