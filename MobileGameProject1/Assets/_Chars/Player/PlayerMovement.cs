﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	private MoJoInputManager mojoIM;

	public float speed = 6f;
	public LayerMask shootableMask;

	Vector3 movement;
	Animator anim;
	Rigidbody playerRigidbody;

	// TODO: Ändern von h und V zu direktem Vektor3
	float jx, jy, wx, wy;  // Soystick und Shooter x,y Coords

	private void Awake() {
		mojoIM = MoJoInputManager.Instance;
		anim = GetComponent<Animator>();
		playerRigidbody = GetComponent<Rigidbody>();

	}

	void FixedUpdate() {
		 jx = Input.GetAxisRaw("Horizontal");
		 jy = Input.GetAxisRaw("Vertical");

		if(mojoIM.joystickInputDirection!=Vector3.zero) { // Wenn Richtungsangabe über den VirtualJoystick kommt
			jx = mojoIM.joystickInputDirection.x;
			jy = mojoIM.joystickInputDirection.z;
		}
		Move(jx, jy);
		Turning();
		Animating(jx, jy);
	}

	void Move (float h, float v) {
		movement.Set(h, 0f, v);
		movement = movement.normalized * speed * Time.deltaTime;
		playerRigidbody.MovePosition(transform.position + movement);
	}

	void Turning() {
        transform.rotation = Quaternion.Euler(0, (mojoIM.wheelAngle+90)*-1, 0);       // Rotation über Wheel mit Winkelangabe
    }

	void Animating (float h, float v)
	{
		bool walking = h != 0f || v != 0f;
		anim.SetBool("IsWalking", walking);
	}
}
