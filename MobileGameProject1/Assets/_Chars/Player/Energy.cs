﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Energy : MonoBehaviour {

    /// <summary>
    /// 
    /// //Dieses Energie-System noch etwas variabler machen, für verschiedene Ressourcen (Mana, Wut, etc..)
    /// Vielleicht hat Alex noch Ideen
    /// 
    /// </summary>
    /// 


    [SerializeField] Slider energySlider;
    [SerializeField] float  maxEnergyPoints=100f;
    [SerializeField] float regenPointsPerSecond = 5f;

    float currentEnergyPoints;

	void Start () {
        currentEnergyPoints = maxEnergyPoints;
	}

    private void Update()
    {
        if (currentEnergyPoints < maxEnergyPoints)
        {
            var poinsToAdd = regenPointsPerSecond * Time.deltaTime;
            currentEnergyPoints = Mathf.Clamp(currentEnergyPoints + poinsToAdd, 0, maxEnergyPoints);
            SetEnergyUI();
        }
    }


    public bool IsEnergyAvailable(float amount)         // Checken ob genug Energy vorhanden ist
    {
        return amount <= currentEnergyPoints;
    }

    public void consumeEnergy(float amount)             // Energy verbrauchen und Balken updaten
    {
        currentEnergyPoints -= amount;
        SetEnergyUI();
    }

    private void SetEnergyUI()
    {
        // Adjust the value and colour of the slider.
        energySlider.value = currentEnergyPoints;
        //fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, currentHealth / startingHealth);
    }

}
