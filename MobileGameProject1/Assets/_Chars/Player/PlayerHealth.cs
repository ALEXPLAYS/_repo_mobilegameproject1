﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class PlayerHealth : MonoBehaviour, IDamagable
{

    [SerializeField] AudioClip[] damageSounds;
    [SerializeField] AudioClip[] deathSounds;

	CombatTextManager sct;
	public Canvas sctCanvas;
	public float maxHealthPoints = 100f;
    public float currentHealth;
    public float healthAsPercentage { get { return currentHealth / maxHealthPoints; } }

	// für die Runde Health bar
    public Slider healthSlider;
	public Image fillImage;
	public Color fullHealthColor = Color.green;
	public Color zeroHealthColor = Color.red;

	// Partikelsysteme mit zugehöriger AudioSource (Audio fehlt gerade...)
	//public GameObject explosionParticlePrefab;
	private ParticleSystem explosionParticles;

	// Der rote Bildschirm overlay wenn man Schaden bekommt
	public Image damageImage;									// Hier brauchen wir eine Referenz zum DMG-Splash-Screen
	public float flashSpeed = 5f;								// Wie schnell kommt der DMG-Splashö-Screen?
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);		// DMG-Splash-Screen-Color

   
    Animator anim;                                              // Animator referenzieren
    AudioSource playerAudio;                                    // AudioSource referenzieren
    PlayerMovement playerMovement;                              // Referenz auf Movement Script
    private bool isDead;
    private bool damaged;


    void Awake ()
    {
		anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        currentHealth = maxHealthPoints;
		isDead = false;
		SetHealthUI();  // HealthBar initialisieren
    }

 
    private void Start()
    {
		sct = CombatTextManager.Instance;
	}


    void Update ()
    {
        if(damaged)	// Schaden bekommen?
        {
            damageImage.color = flashColour;	// rotes flackern bei hits
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;

		if (Input.GetKeyDown("c")) Makesct();

	}


    public void TakeDamage (float amount, Vector3 hitPoint)
    {
        damaged = true;                                                                         // Schaden bekommen
        currentHealth -= amount;                                                                // HP berechnen
        playerAudio.clip = damageSounds[UnityEngine.Random.Range(0,damageSounds.Length)];       // Zufälligen Hit Audioclip setzen
        playerAudio.Play ();                                                                    // Hit-Sound spielen
		SetHealthUI();                                                                          // HP Balken updaten (Amount and Color)
        if(currentHealth <= 0 && !isDead)                                                       // Naja - Tot halt :D
        {
            Death ();
        }
    }

	private void SetHealthUI() {
		// Adjust the value and colour of the slider.

		healthSlider.value = healthAsPercentage*100;
		fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, healthAsPercentage);
	}

		void Makesct()
		{
			bool crit = true;
			sct.CreateText(sctCanvas, "crit", Color.red, crit);
		}

void Death ()
    {
		// Play the effects for the death of the tank and deactivate it.
		isDead = true;
		
        // xxxx    TO DO   playerShooting.DisableEffects ();	// SchiessPartikelEffekt ausschalten
        anim.SetTrigger ("Die");            // Death Animation spielen
										// ExplosionAudio abspielen

        playerAudio.clip = deathSounds[UnityEngine.Random.Range(0, deathSounds.Length)];		// Death Sound clip als Audioquelle setzen
        playerAudio.Play ();				// Audio Spielen
        playerMovement.enabled = false;		// Bewegung ausschalten

        // sxxxxx TO DO   playerShooting.enabled = false;		// Schiessen ausschalten


		// Google Play - Punkte in das Leaderboard posten
		//PlayGamesScript.AddScoreToLeaderboard(GPGSIds.leaderboard_dumdidum, ScoreManager.score);
		//PlayGamesScript.UnlockAchievement(GPGSIds.achievement_tester);
	}


    public void RestartLevel ()
    {
       // SceneManager.LoadScene (0);
    }
}
