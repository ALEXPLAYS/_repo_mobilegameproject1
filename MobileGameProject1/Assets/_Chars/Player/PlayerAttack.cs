﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {


    public Weapon weaponInUse;
    [SerializeField] GameObject weaponHolder;
    public Transform gunEnd;
    public Animation weaponAnimation;

    public Ability ability1;
    public Ability ability2;
    public Ability ability3;
    public Ability ability4;

    public AbilityCooldown specialButton1;
    public AbilityCooldown specialButton2;
    public AbilityCooldown specialButton3;
    public AbilityCooldown specialButton4;


    private void Awake()
    {
        PutWeaponInHand();
    }



    void Start () {
        //inizializeAbilities();
    }


    public void inizializeAbilities()
    {
        ability1.Initialize(gameObject);
        specialButton1.Initialize(ability1);
        //MyPlayerManager.ability1 = ability1;

        ability2.Initialize(gameObject);
        specialButton2.Initialize(ability2);
        //MyPlayerManager.ability2 = ability2;

        ability3.Initialize(gameObject);
        specialButton3.Initialize(ability3);
        // MyPlayerManager.ability3 = ability3;

        ability4.Initialize(gameObject);
        specialButton4.Initialize(ability4);
        // MyPlayerManager.ability4 = ability4;
    }


    public  Animation GetCurrentWeaponAnimationClip()
    {
        return weaponInUse.GetWeaponPrefab().GetComponent<Animation>();
    }


    private void PutWeaponInHand()
    {
        var weaponPrefab = weaponInUse.GetWeaponPrefab();
        var weapon = Instantiate(weaponPrefab, weaponHolder.transform); // Move correct and child to hahnd / weapon Holder
        weapon.transform.localPosition = weaponInUse.gripTransform.localPosition;
        weapon.transform.localRotation = weaponInUse.gripTransform.localRotation;
        weaponAnimation = GetCurrentWeaponAnimationClip();
        gunEnd = weapon.GetComponentInChildren<GunEnd>().transform;
        MyPlayerManager.gunEnd = gunEnd;

        inizializeAbilities();
    }


}
