﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysInRange : MonoBehaviour {

	public float enemysInRangeRadius = 50f;
	public LayerMask shootableMask;

	private void OnTriggerEnter(Collider other) {
		int enemyCount = 0;
		Collider[] colliders = Physics.OverlapSphere(transform.position, enemysInRangeRadius, shootableMask);
		for (int i = 0; i < colliders.Length; i++) {
			Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

			if (!targetRigidbody) continue;
			enemyCount++;
		}
		Debug.Log("Enemys in Range: " + enemyCount);
		//return enemyCount;
	}
}
