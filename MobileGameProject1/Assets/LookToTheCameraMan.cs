﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToTheCameraMan : MonoBehaviour {
	Camera cam;
	// Use this for initialization
	void Start () {
		cam = GameObject.FindObjectOfType<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(cam.transform);
	}
}
